<?php

use App\Http\Controllers\AllocationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\prefrenceController;
use App\Http\Controllers\PrjGroupController;
use App\Http\Controllers\statusController;

Route::get('/forgot-password', [IndexController::class, 'forgotpass']);
Route::get('/login', [AuthController::class, 'create'])->name('login');
Route::post('/login', [AuthController::class, 'store']);
Route::delete('/logout', [AuthController::class, 'destroy']);
Route::get('/admin/Classes', [IndexController::class, 'getAdminClasses']);
Route::get('/admin/Allocations', [IndexController::class, 'getAdminAllocations']);
Route::get('/admin/Users', [IndexController::class, 'getAdminUsers']);
Route::get('/admin/Prj', [IndexController::class, 'getAdminPRJ']);
Route::get('/admin/Courses', [IndexController::class, 'getAdminCourses']);
Route::get('/admin/Modules', [IndexController::class, 'getAdminModules']);
Route::get('/admin/Settings', [IndexController::class, 'getAdminSettings']);

Route::get('/user/preferences', [IndexController::class, 'getUserPrefrence']);
Route::get('/user/coordinator', [IndexController::class, 'getCordinator']);
Route::get('/user/leader', [IndexController::class, 'getLeader']);
Route::get('/user/settings', [IndexController::class, 'getUserSettings']);


//admin route goes here
Route::prefix('admin')
  ->name('admin.')
  ->middleware('role:admin')
  ->group(function () {
    error_log("rendering the page ");
    // Route::get('/', function () {
    //   error_log("admin page");
    //   return redirect('/admin/home');
    // });

    Route::get('/home', [IndexController::class, 'getAdminHome']);
  });

//user route goes here
Route::prefix('user')
  ->name('user.')
  ->middleware('role:user')
  ->group(function () {
    // Route::get('/', function () {
    //   return redirect('/dashboard',[IndexController::class, 'getUserHome']);
    // });

    Route::get('/dashboard',[IndexController::class,'getUserHome']);

  });


Route::get('/', function () {
  $user = Auth::user();
  if ($user) {
    if ($user->role === 'admin') {
      return redirect('/admin/home');
    } else if ($user->role === 'user') {
      return redirect('/user/dashboard');
    }
  } else {
    return redirect('/login');
  }
});

Route::post('/user', [UserController::class, 'addUsers']);
Route::put('/editUser/{user}', [UserController::class, 'editUser']);
Route::delete('/deleteUser/{userID}', [UserController::class, 'deleteUsers']);

Route::post('/addclasses', [ClassesController::class, 'addClass']);
Route::delete('/deleteClass/{class_ID}', [ClassesController::class, 'deleteClass']);
Route::post('/editClassStrength', [ClassesController::class, 'editClassStrength']);

Route::post('/addModule', [ModuleController::class, 'addModule']);
Route::post('/editModule', [ModuleController::class, 'editModule']);
Route::delete('/deleteModule/{class_ID}', [ModuleController::class, 'deleteModule']);

Route::post('/addPrjGroups', [PrjGroupController::class, 'addPrjGroups']);
Route::post('/editPrjGroups', [PrjGroupController::class, 'editPrjGroups']);
Route::delete('/deletePrj/{prj_ID}', [PrjGroupController::class, 'deletePrj']);

Route::post('/addCourses', [CourseController::class, 'addCourses']);
Route::post('/editCourses', [CourseController::class, 'editCourses']);
Route::delete('/deleteCourse/{class_ID}', [CourseController::class, 'deleteCourse']);

Route::post('/statusUpdate', [statusController::class, 'statusUpdate']);
Route::post('/editState', [statusController::class, 'editState']);

Route::post('/editAllocations', [AllocationController::class, 'editAllocations']);
Route::post('/allocate', [AllocationController::class, 'allocate']);

Route::post('/prefrence', [prefrenceController::class, 'set']);
