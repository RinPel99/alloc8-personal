<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('prj_groups', function (Blueprint $table) {
            $table->id();
            $table->string('prj_code')->nullable();;
            $table->string('semester');
            $table->string('guide_name');
            $table->unsignedBigInteger('guide_id');
            $table->year('year');
            $table->string('class_code');
            $table->unsignedBigInteger('class_id');
            $table->timestamps();

            $table->foreign('guide_id')->references('id')->on('users');
            $table->foreign('class_id')->references('id')->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prj_groups');
    }
};
