<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->string('module_code');
            $table->string('module_name');
            $table->string('coordinator_name');
            $table->unsignedBigInteger('module_coordinator_id');
            $table->integer('WeeklyClassHours');
            $table->timestamps();

            $table->foreign('module_coordinator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('modules');
    }
};
