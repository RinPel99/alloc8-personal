<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    use HasFactory;

    protected $fillable = [
        'module_code',
        'module_name',
        'coordinator_name',
        'module_coordinator_id',
        'WeeklyClassHours',
    ];
    protected $primaryKey = 'module_code';
    protected $casts = ['module_code' => 'string'];

}
