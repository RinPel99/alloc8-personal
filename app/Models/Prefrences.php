<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prefrences extends Model
{
    use HasFactory;

    protected $fillable = [
        'rank',
        'semester',
        'year',
        'user_id',
        'user_name',
        'module_code',
    ];
}
