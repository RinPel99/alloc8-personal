<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WeeklyHours extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id','user_name','year','semester','class_hours','prj_groups'
    ];

    public function user(): BelongsTo{
        return $this->belongsTo(App\Models\User::class);
    }
}
