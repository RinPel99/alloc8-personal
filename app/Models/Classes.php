<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'course_name',
        'course_id' ,
        'start_year',
        'end_year', 
        'strength' 
    ];
   
}
