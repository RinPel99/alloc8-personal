<?php

namespace App\Http\Controllers;

use App\Models\PrjGroups;
use App\Models\WeeklyHours;
use Illuminate\Http\Request;

class PrjGroupController extends Controller
{
    public function addPrjGroups(Request $request)
    {
        // Validate the request data
        $validated = $request->validate([
            'semester' => 'required',
            'year' => 'required',
            'guide_name' => 'required',
            'guide_id' => 'required',
            'class_code' => 'required',
            'class_id' => 'required',
        ]);
        
        // Create a new PRJ group
        $prjGroup = PrjGroups::create($validated);
        
        error_log("Test!!!x");
        $prjGroup->prj_code = $validated['class_code'] . $prjGroup->id;

        $prjGroup->save(); // Save the updated prj_code

        $newlyCreatedId = $prjGroup->id;

        // Now, update the Weeklyhours table
        WeeklyHours::where([
            'user_id' => $request->input('guide_id'),
            'year' => $request->input('year'),
            'semester' => $request->input('semester'),
        ])->increment('prj_groups');

        return response()->json($prjGroup, 201);
    }

    public function editPrjGroups(Request $request)
    {
        $prjGroup = PrjGroups::where([
            'id' => $request->input('prj_id'),
        ])->update(
            [
                'guide_name' => $request->input('guideedited'),
                'guide_id' => $request->input('editedguideid'),
            ]
        );

        WeeklyHours::where([
            'user_id' => $request->input('editedguideid'),
            'year' => $request->input('year'),
            'semester' => $request->input('semester'),
        ])->increment('prj_groups');


        WeeklyHours::where([
            'user_id' => $request->input('p_guide_id'),
            'year' => $request->input('year'),
            'semester' => $request->input('semester'),
        ])->decrement('prj_groups');

        return response()->json($prjGroup, 201);
    }
    public function deletePrj($id)
        {
            $item = PrjGroups::find($id);

            if (!$item) {
                return response()->json(['message' => 'User not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'Prj group deleted'], 200);
        }
}
