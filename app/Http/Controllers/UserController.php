<?php

namespace App\Http\Controllers;

use App\Models\User;
use Barryvdh\LaravelIdeHelper\UsesResolver;
use Illuminate\Http\Request;
use Mail;
use Str;

class UserController extends Controller
{
    public function AddUsers(Request $request)
    {
        $data = $request->validate([
            
                'name' => 'required',
                'email' => 'required|email',
        ]);
        $randomPassword = Str::random(8);

        // Add the generated password to the data array
        $data['password'] = bcrypt($randomPassword); // Hash the password

        // Send an email with the random password
        $this->sendPasswordEmail($data['email'], $randomPassword);

        $data['role'] = 'user';

        $user = User::factory()->create($data);

        return response()->json(['message' => 'User added successfully', 'user' => $user], 201);
    }
    private function sendPasswordEmail($email, $password)
    {
        // You can customize the email view and subject as needed
        Mail::send('emails.newUserPassword', ['password' => $password], function ($message) use ($email) {
            $message->to($email)->subject('Your New User Password');
        });
    }
    public function deleteUsers($id)
        {
            $item = User::find($id);

            if (!$item) {
                return response()->json(['message' => 'User not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'User deleted'], 200);
        }

        public function editUser(Request $request, User $user)
        {
            error_log("test");
            // Validate the request data
            $this->validate($request, [
                'name' => 'required|string',
            ]);

            // Update the user's information
            $user->update([
                'name' => $request->input('name'),
            ]);

            return response()->json(['message' => 'User updated successfully']);
        }   
}
