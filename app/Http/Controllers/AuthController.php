<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function create(){
        return inertia(
            'Auth/Create'
        );
    }

    public function store(Request $request){
        $user = User::where('email', $request->input('email'))->first();
    
        if (!$user) {
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ])->onlyInput('email');
        }
    
        $result = Auth::attempt($request->validate([
            'role' => $user->role,
            'email' => 'required|email',
            'password' => 'required'
        ]), true);
    
        if ($result){
            $request->session()->regenerate();
    
            return redirect()->intended('/');
        } else {
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ])->onlyInput('email');
        }
    }

    public function destroy(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
