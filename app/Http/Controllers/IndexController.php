<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Modules;
use App\Models\Prefrences;
use App\Models\PrjGroups;
use App\Models\WeeklyHours;
use App\Models\state;
use App\Models\TeachingRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getAdminHome(){

        return inertia('Admin/Home', [
            'weeklyhours' => WeeklyHours::all()
        ]);
    }
    public function getAdminAllocations(){

        return inertia('Admin/Allocations', [
            'users' => User::where('role', 'user')->get(),
            'classes' => Classes::all(),
            'course' => Course::all(),
            'module' => Modules::all(),
            'teaching_record' => TeachingRecord::all(),
            'status' => state::all(),

        ]);
    }
    public function getAdminClasses(){

        return inertia('Admin/Classes', [
            'classes' => Classes::all(),
            'courses' => Course::all(),
        ]);
    }
    public function getAdminUsers(){

        return inertia('Admin/Users', [
            'users' => User::where('role', 'user')->get(),
            'prefrences' => Prefrences::all(),
        ]);
    }
    public function getAdminPRJ(){

        return inertia('Admin/Prj', [
            'prj' => PrjGroups::all(),
            'classes' => Classes::all(),
            'weeklyhours' => WeeklyHours::all()
        ]);
    }

    public function getAdminCourses(){

        return inertia('Admin/Courses', [
            'course' => Course::all(),
            'modules' => Modules::all(),
            'users' => User::where('role', 'user')->get(),
        ]);
    }
    public function getAdminModules(){

        return inertia('Admin/Modules', [
            'modules' => Modules::all(),
            'users' => User::where('role', 'user')->get(),
        ]);
    }
    public function getAdminSettings(){

        return inertia('Admin/Settings', [
            'users' => User::where('role', 'admin')->get(),
        ]);
    }

    public function getUserHome(){

        return inertia('User/dashboard', [
            'user' => auth()->user(),
            'weeklyhours' => WeeklyHours::all(),
            'teaching' => TeachingRecord::all(),
            'prj' => PrjGroups::all(),
        ]);
    }
    public function getUserPrefrence(){

        return inertia('User/Preference', [
            'user' => auth()->user(),
            'stat' => state::all(),
            'prefrence' => Prefrences::all(),
            'mod' => Modules::all()
        ]);
    }
    public function getCordinator(){

        return inertia('User/Module', [
            'user' => auth()->user(),
            'teaching' => TeachingRecord::all(),
            'mod' => Modules::all()
        ]);
    }
    public function getLeader(){

        return inertia('User/Course', [
            'user' => auth()->user(),
            'teaching' => TeachingRecord::all(),
            'cls' => Classes::all(),
            'course' => Course::all()
        ]);
    }

    public function getUserSettings(){

        return inertia('User/PasswordChange', [
            'user' => auth()->user(),
            'teaching' => TeachingRecord::all()
        ]);
    }

}


