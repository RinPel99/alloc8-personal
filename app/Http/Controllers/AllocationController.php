<?php

namespace App\Http\Controllers;

use App\Models\state;
use App\Models\TeachingRecord;
use App\Models\User;
use App\Models\WeeklyHours;
use Illuminate\Http\Request;
use Mail;

class AllocationController extends Controller
{   
    public function allocate(Request $request)
    {
        $allocations = $request->input('filled', []);

        if (count($allocations) === 0) {
            return response()->json(['error' => 'Filled is empty'], 404);
        } else {
            foreach ($allocations as $item) {
                TeachingRecord::create([
                    'user_id' => $item['user_id'],
                    'userName' => $item['username'],
                    'year' => $request->input('year'),
                    'semester' => $request->input('sem'),
                    'module_code' => $item['module'],
                    'classCode' => $item['class_code'],
                    'classHours' => $item['classhours'],
                    'class_id' => $item['class'],    
                ]);
                    
                $weeklyHours = WeeklyHours::where([
                    'user_id' => $item['user_id'],
                    'year' => $request->input('year'),
                    'semester' => $request->input('sem'),
                    ])->first();
                    
                    if ($weeklyHours) {
        
                        $add = $weeklyHours->class_hours + $item['classhours'];
        
                        $weeklyHours->update([
                            'class_hours' => $add,
                        ]);
                    }
                    else {
                        WeeklyHours::creatre([
                            'user_id' => $item['user_id'],
                            'userName' => $item['username'],
                            'year' => $request->input('year'),
                            'semester' => $request->input('sem'),
                            'class_hours' => $item['classhours'], 
                            'prj_groups' => 0
                        ]);
                    }
                    
                }
        }
        state::where([
            'year' => $request->input('year'),
            'semester' => $request->input('sem'),
        ])->update([
            'state' => false,
        ]);
        
    }
    public function editAllocations(Request $request)
    {
        $u_name = $request->input('userName');
        $u_id = $request->input('user_id');

        $course = TeachingRecord::find($request->input('id'))->first();

        if (!$course) {
            return response()->json(['error' => 'Course not found'], 404);
        }
        $weeklyHoursOld = WeeklyHours::where([
            'user_id' => $course['user_id'],
            'year' => $course['year'],
            'semester' => $course['semester'],
        ])->first();

        $weeklyHoursNew = WeeklyHours::where([
            'user_id' => $course['user_id'],
            'year' => $course['year'],
            'semester' => $course['semester'],
        ])->first();

        if ($weeklyHoursOld) {

            $add = $weeklyHoursOld->class_hours - $course['classhours'];

            $weeklyHoursNew->update([
                'class_hours' => $add,
            ]);
        }
        if ($weeklyHoursNew) {

            $add = $weeklyHoursNew->class_hours + $course['classhours'];

            $weeklyHoursNew->update([
                'class_hours' => $add,
            ]);
        }
        

        // Store the original coordinator ID for comparison
        $originalCoordinatorId = $course->user_id;

        // Check if the coordinator has changed
        if ($originalCoordinatorId != $request->input('user_id')) {
            // Fetch the old coordinator
            $oldCoordinator = User::find($originalCoordinatorId);

            // Send email to the old coordinator
            Mail::send('emails.noLongerTutor', ['module' => $course->module_code, 'class'=> $request->input('class')], function ($message) use ($oldCoordinator) {
                $message->to($oldCoordinator->email)->subject('No Longer a coordinator for the module');
            });

            // Fetch the new coordinator
            $newCoordinator = User::find($request->input('user_id'));

            // Send email to the new coordinator
            Mail::send('emails.newtutor', ['module' => $course->module_code, 'class'=> $request->input('class')], function ($message) use ($newCoordinator) {
                $message->to($newCoordinator->email)->subject('New coordinator for the module');
            });
        }
        $course->update([
            'userName' => $u_name,
            'user_id' => $u_id,
        ]);

        return response()->json($course, 200);
    }
}
