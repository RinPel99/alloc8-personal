<?php

namespace App\Http\Controllers;

use App\Models\Modules;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;
use Psy\Readline\Hoa\Console;
use Validator;

class ModuleController extends Controller
{
    public function addModule(Request $request)
    {
        // Validate the request data
        $request->validate([
            'u_name' => 'required',
            'u_id' => 'required', 
            'c_hrs' => 'required',
            'm_code' => 'required',
            'm_name' => 'required',
        ]);
    
        // Create a new class
        Modules::create([
            'module_code' => $request->input('m_code'),
            'module_name' => $request->input('m_name'),
            'coordinator_name' => $request->input('u_name'),
            'module_coordinator_id' => $request->input('u_id'),
            'WeeklyClassHours' => $request->input('c_hrs'),
        ]);
        $newCoordinator = User::find($request->input('u_id'));

        Mail::send('emails.newCoordinator', ['module' => $request->input('m_code')], function ($message) use ($newCoordinator) {
            $message->to($newCoordinator->email)->subject('New coordinator for the module');
        });
    
        // Redirect back to the current page with a success message
        return redirect()->back()->with('success', 'Class added successfully');
    }

    public function editModule(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'u_id' => 'required',
            'u_name' => 'required',
            'c_hrs' => 'required|numeric',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $module = Modules::where('module_code', $request->input('id'))->first();

        if (!$module) {
            return response()->json(['error' => 'Module not found'], 404);
        }

        // Store the original coordinator ID for comparison
        $originalCoordinatorId = $module->module_coordinator_id;

        // Check if the coordinator has changed
        if ($originalCoordinatorId != $request->input('u_id')) {
            // Fetch the old coordinator
            $oldCoordinator = User::find($originalCoordinatorId);

            // Send email to the old coordinator
            Mail::send('emails.noLongerCoordinator', ['module' => $module->module_code], function ($message) use ($oldCoordinator) {
                $message->to($oldCoordinator->email)->subject('No Longer a coordinator for the module');
            });

            // Fetch the new coordinator
            $newCoordinator = User::find($request->input('u_id'));

            // Send email to the new coordinator
            Mail::send('emails.newCoordinator', ['module' => $module->module_code], function ($message) use ($newCoordinator) {
                $message->to($newCoordinator->email)->subject('New coordinator for the module');
            });
        }
        $module->update([
            'coordinator_name' => $request->input('u_name'),
            'module_coordinator_id' => $request->input('u_id'),
            'WeeklyClassHours' => $request->input('c_hrs'),
        ]);

        return response()->json($module, 200);
    }

    public function deleteModule($module_code)
        {
            $item = Modules::where([
                'module_code' => $module_code
            ]);

            if (!$item) {
                return response()->json(['message' => 'Module not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'Module deleted'], 200);
        }
    }
