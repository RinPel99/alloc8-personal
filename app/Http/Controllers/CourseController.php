<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;
use Validator;

class CourseController extends Controller
{
    public function addCourses(Request $request)
    {
        // Validate incoming data
        $request->validate([
            'course_name' => 'required|string',
            'course_leader_name' => 'required|string',
            'course_leader_id' => 'required|numeric',
        ]);

        // Check if 'modules' key is present and is an array
        $modules = $request->input('modules', []);

        $semRearranged = $this->rearrangeSemesters($modules);
        $cname = $request->input('course_name');
        $cl_name = $request->input('course_leader_name');
        $cl_id = $request->input('course_leader_id');

        // Convert semRearranged to a JSON string
        $semRearrangedJson = json_encode($semRearranged);

        $course = Course::create([
            'course_name' => $cname,
            'semesters' => $semRearrangedJson, // Insert the JSON string
            'course_leader_name' => $cl_name,
            'course_leader_id' => $cl_id,
        ]);

        $newCoordinator = User::find($request->input('course_leader_id'));

        Mail::send('emails.newLeader', ['course' => $cname], function ($message) use ($newCoordinator) {
            $message->to($newCoordinator->email)->subject('New Program Leader');
        });

        return response()->json($course, 201);
    }

    public function editCourses(Request $request)
    {
        $request->validate([
            'course_name' => 'required|string',
            'course_leader_name' => 'required|string',
            'course_leader_id' => 'required|numeric',
        ]);

        // Check if 'modules' key is present and is an array
        $modules = $request->input('modules', []);

        $semRearranged = $this->rearrangeSemesters($modules);
        $cname = $request->input('course_name');
        $cl_name = $request->input('course_leader_name');
        $cl_id = $request->input('course_leader_id');

        // Convert semRearranged to a JSON string
        $semRearrangedJson = json_encode($semRearranged);

        $course = Course::where('id', $request->input('id'))->first();

        if (!$course) {
            return response()->json(['error' => 'Course not found'], 404);
        }

        // Store the original coordinator ID for comparison
        $originalCoordinatorId = $course->course_leader_id;

        // Check if the coordinator has changed
        if ($originalCoordinatorId != $request->input('course_leader_id')) {
            // Fetch the old coordinator
            $oldCoordinator = User::find($originalCoordinatorId);

            // Send email to the old coordinator
            Mail::send('emails.noLongerLeader', ['course' => $course->module_code], function ($message) use ($oldCoordinator) {
                $message->to($oldCoordinator->email)->subject('No Longer a coordinator for the module');
            });

            // Fetch the new coordinator
            $newCoordinator = User::find($request->input('course_leader_id'));

            // Send email to the new coordinator
            Mail::send('emails.newLeader', ['course' => $course->module_code], function ($message) use ($newCoordinator) {
                $message->to($newCoordinator->email)->subject('New coordinator for the module');
            });
        }
        $course->update([
            'course_name' => $cname,
            'semesters' => $semRearrangedJson, // Insert the JSON string
            'course_leader_name' => $cl_name,
            'course_leader_id' => $cl_id,
        ]);

        return response()->json($course, 200);
    }

    private function rearrangeSemesters($data)
    {
        $rearranged = [];

        // Iterate through each semester
        for ($i = 1; $i <= 8; $i++) {
            // Get the current semester data
            $semesterData = $data[$i] ?? [];

            // Make sure $semesterData is an array
            if (!is_array($semesterData)) {
                $semesterData = [];
            }

            // Remove empty values and bring forward non-empty values
            $rearranged[$i] = array_merge(array_filter($semesterData), array_fill(0, 6, ''));
        }

        return $rearranged;
    }

    public function deleteCourse($course_id)
        {
            $item = Course::where([
                'id' => $course_id
            ]);

            if (!$item) {
                return response()->json(['message' => 'Module not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'Module deleted'], 200);
        }
    

}
