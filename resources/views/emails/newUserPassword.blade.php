<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Alloc8</title>
</head>

<body class="ms-5"
    style="background-image: linear-gradient(to right bottom, #80b561, #92af4a, #a5a834, #b99f21, #ce9315, #d9961b, #e49a21, #ef9d27, #f2b233, #f4c843, #f5dc56, #f6f16b);">

    <div class="container d-flex justify-content-center row text-center align-item-center ps-5">
        <!-- logo -->
        <div class="container pt-5">
            <img src="./../../js/Components/logo.png" alt="logo" class="logo" style="height: min-content;">
        </div>
        <!-- main body  -->
        <div class="container py-3">
            <h1 class="">Wellcome to Alloc8!</h1>
            <div class="container pb-5">
                <h3>Your Alloc8 Account has been created SUCCESSFULLY!!</h3>
                <p>We are pleased to inform you that an account has been created for <br>
                    you with the Alloc8 system. To get started, please use the <b>default password </b>provided below.
                    <br>
                    We recommend that <b>you change this password to a more secure
                        one that you can remember in the future. </b>
                </p>
                <hr>

                <div class="container shadow py-3 cust-div">
                    <h4 style="color:white;">Your Default Password: {{ $password }}</h4>
                </div>
                <hr>

                <br>

                To change your password, please follow these steps: <br>
                <ul class="d-flex justify-content-center row">
                    <li class="d-flex justify-content-center">
                        Log in to your account at: <a href="https://kheydrupchoney.serv00.net/"> <i
                                class="fa fa-eye">kheydrupchoney.serv00.net</i>.</a>
                    </li>
                    <li class="d-flex justify-content-center">
                        Go to your account settings and select the option to change your password.
                    </li>
                    <li class="d-flex justify-content-center">
                        Follow the instructions to create a new, secure password that you can remember.
                    </li>
                </ul>
                <p> If you encounter any issues or need assistance, please contact our support team at:</p>
                <ul>
                    <li class="d-flex justify-content-center">
                        Email: <a href="javascript: void(0)"
                            onclick="location.href='mailto:2023css1g6.gcit@rub.edu.bt';"><i class="fa fa-envelope">
                                2023css1g6.gcit@rub.edu.bt</i> </a>
                    </li>
                    <li class="d-flex justify-content-center">
                        Phone: <a href="tel:+97577882338"><i class="fa fa-phone"> +975 77882338</i></a>
                    </li>
                </ul>
                <hr>
                <div class="footer">

                    <!-- Grid column -->
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold">Follow us</h6>

                        <!-- Facebook -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #3b5998;" href="#!"
                            role="button"><i class="fa fa-facebook"></i></a>

                        <!-- Twitter -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #55acee" href="#!"
                            role="button"><i class="fa fa-twitter"></i></a>

                        <!-- Google -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #dd4b39" href="#!"
                            role="button"><i class="fa fa-google"></i></a>

                        <!-- Instagram -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #ac2bac" href="#!"
                            role="button"><i class="fa fa-instagram"></i></a>

                        <!-- Linkedin -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #0082ca" href="#!"
                            role="button"><i class="fa fa-linkedin"></i></a>
                        <!-- Github -->
                        <a class="btn btn-primary btn-floating m-1" style="background-color: #333333" href="#!"
                            role="button"><i class="fa fa-github"></i></a>
                    </div>
                </div>
                </section>
            </div>
            <hr>
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); border-radius: 10px;">
                © 2023 Copyright:
                <a class="text-white" href="https://kheydrupchoney.serv00.net/">Alloc8.gcit.bt</a>
            </div>
        </div>


    </div>

    </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>
<style scoped>
    a {
        text-decoration: none;
        color: inherit;
    }

    a:hover {
        text-decoration: None;
        color: white;
        transition: ease-in;
    }

    li {
        text-decoration: none;
    }

    .cust-div {
        background-color: rgba(0, 0, 0, 0.2);
        border-radius: 10px;
        font-family: cursive;
    }

    .cust-div:hover {
        background-color: black;
        border-radius: 10% 0% 10% 0% / 100% 0% 100% 0%;


    }
</style>