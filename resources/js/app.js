import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import MainLayout from './Layouts/MainLayout.vue'
import '../css/app.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { faArrowLeft, faUserSecret, faFolder} from '@fortawesome/free-solid-svg-icons'
import { faFacebook,faXTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'

/* add icons to the library */
library.add(faArrowLeft, faUserSecret, faFolder,faXTwitter, faFacebook, faInstagram)

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
    let page = pages[`./Pages/${name}.vue`]
    if (name.startsWith('User/') || name.startsWith('Admin/') ){
      page.default.layout =  MainLayout
    }
    
    return page
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .component('Icon', FontAwesomeIcon)
      .mount(el)
  },
})